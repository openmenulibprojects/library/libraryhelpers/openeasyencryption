package it.lundstedt.erik.encryption;

public class main
{
public static void main(String[] args) {
String message="hello there";
final String secretKey="secret";
	
	String encryptedString = AES.encrypt(message,secretKey);
	String decryptedString = AES.decrypt(encryptedString,secretKey) ;
	System.out.println(encryptedString);
	System.out.println(decryptedString);
}
}
